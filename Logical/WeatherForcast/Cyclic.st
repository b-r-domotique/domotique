
PROGRAM _CYCLIC
	Weather;
	
	DomWeather_0(MpLink := ADR(gAlarmXCore), MpLinkCfg:= ADR(gRecipeXml), Enable:= TRUE, PVName:= 'Weather');
	
	MpAlarmXCore_0(MpLink := ADR(gAlarmXCore), Enable := TRUE);
	
	MpRecipeXml_0(MpLink := ADR(gRecipeXml), Enable := TRUE, DeviceName := ADR('CONFIG'), FileName := ADR('DomConfig'));
	

END_PROGRAM
