
PROGRAM _CYCLIC
	UsbNodeListGet_0();
	CASE state OF
		
		0: // Get USB Node list
			UsbNodeListGet_0.enable 	:= TRUE;
			UsbNodeListGet_0.pBuffer 	:= ADR(usbNodeList);
			UsbNodeListGet_0.bufferSize := SIZEOF(usbNodeList);
			CASE UsbNodeListGet_0.status OF 
				0:
					UsbNodeListGet_0.enable 	:= FALSE;
					state := state + 1;
			END_CASE
		1:
			UsbNodeGet_0();
			
	END_CASE
	 
END_PROGRAM
